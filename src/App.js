import React, { Component } from 'react';
import logo from './logo.svg';
import SoundList from './components/SoundList'
import PlayBar from './components/PlayBar'
import FontAwesome from 'react-fontawesome';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
        <FontAwesome
        className='music'
        name='music'
        size='5x'
        style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)', color: '#37474F' }}
        />
        <h2>Welcome to Colb!</h2>
        </div>
        <div className="card card-1">
          <SoundList />
          <button>play</button>
        </div>
        <div className="card card-1 card-blue">
          <SoundList />
          <button>play</button>
        </div>
        <PlayBar/>
      </div>
    );
  }
}


export default App;
