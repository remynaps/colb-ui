import React, { Component } from 'react';
import './PlayBar.css';
import axios from 'axios';
import FontAwesome from 'react-fontawesome';

class PlayBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      playSound: false,
    };
    this.audio = []

    this.play = this.play.bind(this);
  }

  play(e) {
    this.setState({
        playSound: true,
    }, function(){
      console.log("play sound 1");
      var audioElement = document.getElementById('currentSound');
      audioElement.setAttribute("preload", "auto");
      audioElement.autobuffer = true;
      audioElement.play();
      console.log("play sound 2");
    });
  }

  pause() {
    if(this.state.playSound){
        this.setState({
            playSound: false,
        }, function(){
            console.log("stop sound 1");
            var audioElement = document.getElementById('currentSound');
            audioElement.pause();
            console.log("stop sound 2");
        });
    }
  }

  render() {
    return (
      <div className='Bar'>
        <audio id='currentSound'
          src='http://localhost:8080/music/test.mp3' type="audio/mp3">
        </audio>
        <button className='circleButton'>
            <FontAwesome
          className='playButton'
          name='backward'
          size='2x'
        />
        </button>
        <button className='circleButton' onClick={this.play}>
            <FontAwesome
          className='playButton'
          name='play'
          size='2x'
          style={{ textShadow: '0 1px 0 rgba(0, 0, 0, 0.1)'}}
        />
        </button>
        <button className='circleButton'>
            <FontAwesome
          className='playButton'
          name='forward'
          size='2x'
        />
        </button>
      </div>
    );
  }

  componentDidMount() {
    axios.get(`http://localhost:8080/music/test.mp3`)
      .then(res => {
        this.audio = res.data;
      });
  }
}
export default PlayBar;
