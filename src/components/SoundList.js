import React, { Component } from 'react';
import axios from 'axios';

class SoundList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sound: []
    };
  }

  componentDidMount() {
    axios.get(`http://localhost:8080/sounds`)
      .then(res => {
        this.setState({ sound: res.data });
      });
  }

  render() {
    return (
      <div style={{ margin:'20px' }}>
        <form>
          <input type="text" placeholder="Search..." />
          <p>
          </p>
        </form>
        <ul>
          {this.state.sound.map(sound =>
            <li key={sound.Name}>{sound.Words}</li>
          )}
        </ul>
      </div>
    );
  }
}
export default SoundList;
